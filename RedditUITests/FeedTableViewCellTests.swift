//
//  FeedTableViewCellTests.swift
//  Reddit
//
//  Created by Thanh KFit on 5/21/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import XCTest
import Nimble

class FeedTableViewCellTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()
        
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    /*
     Open app
     Tap "Add new post" button
     Fill in "abc" and post
     -> Expect to see the cell
     */
    
    func testCellIsAppearAdequately() {
        let app = XCUIApplication()
        app.buttons["home_add_new_post_button"].tap()
        app.textViews["post_text_view"].typeText("abc")
        app.navigationBars["navigation_bar"].buttons["post_post_button"].tap()
        let tableView = app.tables.element(boundBy: 0)
        let cell = tableView.cells.element(boundBy: 0)
        expect(cell.staticTexts["feed_cell_content_label"].label) == "abc"
        expect(cell.staticTexts["feed_cell_vote_count_label"].label) == "0"
        expect(cell.buttons["feed_cell_up_button"].exists) == true
        expect(cell.buttons["feed_cell_down_button"].exists) == true
    }
    
    func testCellUpVoteButtonIsWorkingWithOneCell() {
        let app = XCUIApplication()
        app.buttons["home_add_new_post_button"].tap()
        app.textViews["post_text_view"].typeText("test")
        app.navigationBars["navigation_bar"].buttons["post_post_button"].tap()
        let tableView = app.tables.element(boundBy: 0)
        let cell = tableView.cells.element(boundBy: 0)
        cell.buttons["feed_cell_up_button"].tap()
        expect(cell.staticTexts["feed_cell_vote_count_label"].label) == "1"
    }
    
    func testCellDownVoteButtonIsWorkingWithOneCell() {
        let app = XCUIApplication()
        app.buttons["home_add_new_post_button"].tap()
        app.textViews["post_text_view"].typeText("test")
        app.navigationBars["navigation_bar"].buttons["post_post_button"].tap()
        let tableView = app.tables.element(boundBy: 0)
        let cell = tableView.cells.element(boundBy: 0)
        cell.buttons["feed_cell_down_button"].tap()
        expect(cell.staticTexts["feed_cell_vote_count_label"].label) == "-1"
    }
    
    /*
     Open app
     Tap "Auto post" twice to create 2 feeds
     Up vote the 2rd feed
     -> Expect to see the 2rd feed will change to 1 vote and then move to 1st position
     */
    func testCellUpVoteButtonIsWorkingWithTwoCell() {
        let app = XCUIApplication()
        app.buttons["home_auto_post_button"].tap()
        app.buttons["home_auto_post_button"].tap()
        
        getCellAtIndex(1).buttons["feed_cell_up_button"].tap()
        expect(self.getCellAtIndex(0).staticTexts["feed_cell_vote_count_label"].label) == "1"
        expect(self.getCellAtIndex(0).staticTexts["feed_cell_content_label"].label) == "Feed 1"
        expect(self.getCellAtIndex(1).staticTexts["feed_cell_vote_count_label"].label) == "0"
        expect(self.getCellAtIndex(1).staticTexts["feed_cell_content_label"].label) == "Feed 0"
    }
    
    /*
     Open app
     Tap "Auto post" twice to create 2 feeds
     Down vote the 1st feed
     -> Expect to see the 1st feed will change to -1 vote and then move to 2rd position
     */
    func testCellDownVoteButtonIsWorkingWithTwoCell() {
        let app = XCUIApplication()
        app.buttons["home_auto_post_button"].tap()
        app.buttons["home_auto_post_button"].tap()
        
        getCellAtIndex(0).buttons["feed_cell_down_button"].tap()
        expect(self.getCellAtIndex(0).staticTexts["feed_cell_vote_count_label"].label) == "0"
        expect(self.getCellAtIndex(0).staticTexts["feed_cell_content_label"].label) == "Feed 1"
        expect(self.getCellAtIndex(1).staticTexts["feed_cell_vote_count_label"].label) == "-1"
        expect(self.getCellAtIndex(1).staticTexts["feed_cell_content_label"].label) == "Feed 0"
    }
    
    /*
     Open app
     Tap "Auto post" 25 times
     
     -> Expect to see 20 feeds
     */
    func testComplicatedCase() {
        let app = XCUIApplication()
        for _ in 0...25 {
            app.buttons["home_auto_post_button"].tap()
        }
        
        self.getCellAtIndex(0).buttons["feed_cell_down_button"].tap()
        self.getCellAtIndex(0).buttons["feed_cell_down_button"].tap()
        getCellAtIndex(1).buttons["feed_cell_up_button"].tap()
        getCellAtIndex(0).buttons["feed_cell_up_button"].tap()
        getCellAtIndex(1).buttons["feed_cell_up_button"].tap()
        getCellAtIndex(0).buttons["feed_cell_up_button"].tap()
        expect(self.getCellAtIndex(0).staticTexts["feed_cell_content_label"].label) == "Feed 3"
        expect(self.getCellAtIndex(0).staticTexts["feed_cell_vote_count_label"].label) == "3"
        expect(self.getCellAtIndex(1).staticTexts["feed_cell_content_label"].label) == "Feed 2"
        expect(self.getCellAtIndex(1).staticTexts["feed_cell_vote_count_label"].label) == "1"
        expect(self.getCellAtIndex(18).staticTexts["feed_cell_content_label"].label) == "Feed 20"
        expect(self.getCellAtIndex(18).staticTexts["feed_cell_vote_count_label"].label) == "0"
        expect(self.getCellAtIndex(19).staticTexts["feed_cell_content_label"].label) == "Feed 21"
        expect(self.getCellAtIndex(19).staticTexts["feed_cell_vote_count_label"].label) == "0"
    }
    
    
}

extension FeedTableViewCellTests {
    func getCellAtIndex(_ index: UInt) -> XCUIElement {
        let app = XCUIApplication()
        let tableView = app.tables.element(boundBy: 0)
        return tableView.cells.element(boundBy: index)
    }
}
