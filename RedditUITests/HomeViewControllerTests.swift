//
//  HomeViewControllerTests.swift
//  Reddit
//
//  Created by Thanh KFit on 5/21/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import XCTest
import Nimble

class HomeViewControllerTests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    /*
     Open app
     -> Expect to see empty feed view
     */
    func testShowEmptyFeedViewAtBegining() {
        let app = XCUIApplication()
        expect(app.otherElements["home_empty_feed_view"].exists) == true
        expect(app.tables.count) == 1
    }
    
}

extension HomeViewControllerTests {
    /*
     Open app
     Tap "Add new post" button
     Enter "abc" on text view
     Tap "Post" button
     -> Expect to see only one feed and its content is "abc" with 0 vote
     */
    func testAddNewFeedButton() {
        let app = XCUIApplication()
        app.buttons["home_add_new_post_button"].tap()
        app.textViews["post_text_view"].typeText("abc")
        app.navigationBars["navigation_bar"].buttons["post_post_button"].tap()
        
        let tableView = app.tables.element(boundBy: 0)
        expect(tableView.cells.count) == 1
        
        let cell = tableView.cells.element(boundBy: 0)
        expect(cell.staticTexts["feed_cell_content_label"].label) == "abc"
        expect(cell.staticTexts["feed_cell_vote_count_label"].label) == "0"
    }
    
    /*
     Open app
     Tap "Auto post" button
     -> Expect to see only one feed and its content is "Feed 0" with 0 vote
     */
    func testAutoPost() {
        let app = XCUIApplication()
        app.buttons["home_auto_post_button"].tap()
        
        expect(app.otherElements["home_empty_feed_view"].exists) == false
        
        let tableView = app.tables.element(boundBy: 0)
        expect(tableView.cells.count) == 1
        
        let cell = tableView.cells.element(boundBy: 0)
        expect(cell.staticTexts["feed_cell_content_label"].label) == "Feed 0"
        expect(cell.staticTexts["feed_cell_vote_count_label"].label) == "0"
    }
    
    /*
     Open app
     Tap "Auto post" button twice
     -> Expect to see 2 feeds
     */
    func testAutoPostTwice() {
        let app = XCUIApplication()
        app.buttons["home_auto_post_button"].tap()
        app.buttons["home_auto_post_button"].tap()
        
        let tableView = app.tables.element(boundBy: 0)
        expect(tableView.cells.count) == 2
    }
    
    /*
     Open app
     Tap "Auto post" 20 times
     -> Expect to see 20 feeds
     */
    func testAutoPost20Times() {
        let app = XCUIApplication()
        for _ in 0...19 {
            app.buttons["home_auto_post_button"].tap()
        }
        
        let tableView = app.tables.element(boundBy: 0)
        expect(tableView.cells.count) == 20
    }
    
    /*
     Open app
     Tap "Auto post" 25 times
     -> Expect to see 20 feeds
     */
    func testAutoPost25Times() {
        let app = XCUIApplication()
        for _ in 0...24 {
            app.buttons["home_auto_post_button"].tap()
        }
        
        let tableView = app.tables.element(boundBy: 0)
        
        expect(tableView.cells.count) == 20
    }

}
