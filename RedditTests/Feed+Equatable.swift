//
//  Feed+Equatable.swift
//  Reddit
//
//  Created by Thanh KFit on 5/19/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation

extension Feed: Equatable {
    public static func ==(lhs: Feed, rhs: Feed) -> Bool {
        return lhs.id == rhs.id && lhs.content == rhs.content && lhs.voteCount == rhs.voteCount
    }
}
