//
//  FeedVotingServiceDefaultTest.swift
//  Reddit
//
//  Created by Thanh KFit on 5/19/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import XCTest
import RxSwift
import Nimble

class FeedVotingServiceDefaultTest: XCTestCase {
    
    var feedVotingServiceOwner: FeedVotingServiceDefault!
    var allFeedsVariableOwner: Variable<[Feed]>!
    
    override func setUp() {
        super.setUp()
        allFeedsVariableOwner = Variable<[Feed]>([])
        feedVotingServiceOwner = FeedVotingServiceDefault(allFeedsVariableOwner: allFeedsVariableOwner)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAddNewAutoPostFeed() {
        let counter = allFeedsVariableOwner.value.count
        feedVotingServiceOwner.addNewAutoPostFeed()
        let allFeeds = allFeedsVariableOwner.value
        
        // Test counter
        expect(allFeeds.count) == counter + 1
        expect(allFeeds).to(containElementSatisfying({ (feed) -> Bool in
            let expectingNewFeed = Feed(id: counter, content: "Feed \(counter)", voteCount: 0)
            print("Thanh0> \(feed.content), \(feed.id), \(feed.voteCount)")
            return feed == expectingNewFeed
        }))
    }
    
    func testAddNewFeed() {
        let counter = allFeedsVariableOwner.value.count
        feedVotingServiceOwner.addNewFeed("Testing")
        
        let allFeeds = allFeedsVariableOwner.value
        expect(allFeeds.count) == counter + 1
        expect(allFeeds).to(containElementSatisfying({ (feed) -> Bool in
            let expectingNewFeed = Feed(id: counter, content: "Testing", voteCount: 0)
            print("Thanh1> \(feed.content), \(feed.id), \(feed.voteCount)")
            return feed == expectingNewFeed
        }))
    }
    
    func testUpVoteFeed() {
        feedVotingServiceOwner.addNewAutoPostFeed()
        let counter = allFeedsVariableOwner.value.count
        let testFeed = allFeedsVariableOwner.value.first!
        
        feedVotingServiceOwner.updateFeed(0, action: VoteAction.upvote)
        let allFeeds = allFeedsVariableOwner.value
        
        expect(allFeeds.count) == counter
        expect(allFeeds).to(containElementSatisfying({ (feed) -> Bool in
            let expectingNewFeed = Feed(id: testFeed.id, content: testFeed.content, voteCount: testFeed.voteCount + 1)
            print("Thanh2> \(feed.content), \(feed.id), \(feed.voteCount)")
            return feed == expectingNewFeed
        }))
    }

    func testDownVoteFeed() {
        // To make sure we always have at least one feed
        feedVotingServiceOwner.addNewAutoPostFeed()
        let counter = allFeedsVariableOwner.value.count
        let testFeed = allFeedsVariableOwner.value.first!
        
        feedVotingServiceOwner.updateFeed(0, action: VoteAction.downvote)
        let allFeeds = allFeedsVariableOwner.value
        
        expect(allFeeds.count) == counter
        expect(allFeeds).to(containElementSatisfying({ (feed) -> Bool in
            let expectingNewFeed = Feed(id: testFeed.id, content: testFeed.content, voteCount: testFeed.voteCount - 1)
            print("Thanh3> \(feed.content), \(feed.id), \(feed.voteCount)")
            return feed == expectingNewFeed
        }))
    }
    
    // Test empty list
    func testSortFeedList1() {
        let input: [Feed] = [Feed]()
        let output = feedVotingServiceOwner.sortFeedList(feeds: input)
        expect(output.count) == 0
    }

    func testSortFeedList2() {
        let input: [Feed] = [Feed(id: 0, content: "Feed 0", voteCount: 0)]
        let expectedOutput = [Feed(id: 0, content: "Feed 0", voteCount: 0)]
        
        let output = feedVotingServiceOwner.sortFeedList(feeds: input)
        expect(output.count).to(equal(expectedOutput.count))
    
        for i in 0...expectedOutput.count - 1 {
            expect(output[i]).to(equal(expectedOutput[i]))
        }
    }
    
    func testSortFeedList3() {
        let input: [Feed] = [
            Feed(id: 0, content: "Feed 0", voteCount: 0)
            , Feed(id: 1, content: "Feed 1", voteCount: 10)
            , Feed(id: 2, content: "Feed 2", voteCount: -5)
            , Feed(id: 3, content: "Feed 3", voteCount: 2)
        ]
        
        let expectedOutput: [Feed] = [
            Feed(id: 1, content: "Feed 1", voteCount: 10)
            , Feed(id: 3, content: "Feed 3", voteCount: 2)
            , Feed(id: 0, content: "Feed 0", voteCount: 0)
            , Feed(id: 2, content: "Feed 2", voteCount: -5)
        ]
        
        let output = feedVotingServiceOwner.sortFeedList(feeds: input)
        expect(output.count).to(equal(expectedOutput.count))
        
        for i in 0...expectedOutput.count - 1 {
            expect(output[i]).to(equal(expectedOutput[i]))
        }
    }
}
