//
//  TestObject+DefaultOnNil.swift
//  Reddit
//
//  Created by Thanh KFit on 5/20/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import XCTest
import Nimble

class TestObject_DefaultOnNil: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testNilString() {
        let text: String? = nil
        expect(text.emptyOnNil()) == ""
    }
    
    func testNormalString() {
        let text: String? = "test"
        expect(text.emptyOnNil()) == "test"
    }
}
