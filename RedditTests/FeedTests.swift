//
//  FeedTests.swift
//  Reddit
//
//  Created by Thanh KFit on 5/19/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import XCTest
import Nimble
import RxSwift

class FeedTests: XCTestCase {
    
    var allFeedsVariableOwner: Variable<[Feed]>!
    
    override func setUp() {
        super.setUp()
        allFeedsVariableOwner = Variable<[Feed]>([])
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInitDefault() {
        let feed = Feed(id: 10, content: "Testing", voteCount: -5)
        expect(feed.id) == 10
        expect(feed.content) == "Testing"
        expect(feed.voteCount) == -5
    }
    
    func testInitUpVote() {
        let previousFeed = Feed(id: 10, content: "Testing", voteCount: -5)
        let feed = Feed(previousFeed: previousFeed, action: VoteAction.upvote)
        
        expect(feed.id) == 10
        expect(feed.content) == "Testing"
        expect(feed.voteCount) == -4
    }
    
    func testInitDownVote() {
        let previousFeed = Feed(id: 10, content: "Testing", voteCount: -5)
        let feed = Feed(previousFeed: previousFeed, action: VoteAction.downvote)
        
        expect(feed.id) == 10
        expect(feed.content) == "Testing"
        expect(feed.voteCount) == -6
    }
    
    func testAutoCreateFeed() {
        let counter = allFeedsVariableOwner.value.count
        let feed = Feed.autoCreatFeed(allFeedsVariable: allFeedsVariableOwner)
        
        expect(feed.id) == counter
        expect(feed.content) == "Feed \(counter)"
        expect(feed.voteCount) == 0
    }
}
