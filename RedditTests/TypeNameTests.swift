//
//  TypeNameTests.swift
//  Reddit
//
//  Created by Thanh KFit on 5/19/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import XCTest
import Nimble

class TypeNameTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testTypeName() {
        expect(HomeViewController.typeName) == "HomeViewController"
        expect(HomeViewControllerViewModel.typeName) == "HomeViewControllerViewModel"
    }
    
}
