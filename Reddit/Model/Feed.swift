//
//  Feed.swift
//  Reddit
//
//  Created by Thanh KFit on 5/18/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation
import RxSwift

class Feed {
    let id: Int
    let content: String
    let voteCount: Int
    
    init(
        id: Int
        , content: String
        , voteCount: Int
        ) {
        self.id = id
        self.content = content
        self.voteCount = voteCount
    }
    
    init(
        previousFeed: Feed
        , action: VoteAction
        ) {
        self.id = previousFeed.id
        self.content = previousFeed.content
        self.voteCount = previousFeed.voteCount + action.voteEffectiveValue
    }
    
    class func autoCreatFeed(allFeedsVariable: Variable<[Feed]> = allFeedsVariableSharing) -> Feed {
        let count = allFeedsVariable.value.count
        return Feed(id: count, content: "Feed \(count)", voteCount: 0)
    }
}

