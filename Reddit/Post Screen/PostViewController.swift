//
//  PostViewController.swift
//  Reddit
//
//  Created by Thanh KFit on 5/18/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class PostViewController: ViewController {
    
    var viewModel: PostViewControllerViewModel! = PostViewControllerViewModel()
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textViewPlaceHolderLabel: UILabel!
    @IBOutlet weak var postButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bind()
        textView.becomeFirstResponder()
    }

    // Add new post then move to homepage.
    // Show the popup if the text is empty or its length is more than 255
    @IBAction func didTapPostButton(_ sender: Any) {
        let checking = viewModel.textViewChecking.value
        
        switch checking {
        case .empty, .exceed:
            let alertController = UIAlertController(title: "", message: checking.popupMessage, preferredStyle: UIAlertControllerStyle.alert)
            let cancelAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.cancel, handler: nil)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        case .normal:
            viewModel.didTapPostButton.onNext()
            tabBarController?.selectedIndex = 0
        }
    }
    
    // Move to home page and reset the text
    // Show the popup if the text is not empty
    @IBAction func didTapCancelButton(_ sender: Any) {
        if textView.text.isEmpty {
            tabBarController?.selectedIndex = 0
        } else {
            let alertController = UIAlertController(title: "Confirm", message: "Your draft has not been saved. Are you sure to cancel?", preferredStyle: UIAlertControllerStyle.alert)
            let noAction = UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil)
            let yesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { [weak self] (_) in
                self?.viewModel.resetTextView.onNext()
                self?.tabBarController?.selectedIndex = 0
            }
            alertController.addAction(noAction)
            alertController.addAction(yesAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
}

extension PostViewController: ViewModelBindable {
    func bind() {
        textView.rx.text
            .subscribe(
                onNext: { [weak self] (text: String?) in
                    self?.viewModel.textViewText.value = text.emptyOnNil()
                }
            ).addDisposableTo(disposeBag)
        
        viewModel
            .resetTextView
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self]() in
                self?.textView.text = ""
            })
            .addDisposableTo(disposeBag)
        
        viewModel
            .postButtonIsEnable
            .asDriver()
            .drive(onNext: { [weak self] (isEnable) in
                if isEnable {
                    self?.postButton.tintColor = UIColor.redditBlue()
                } else {
                    self?.postButton.tintColor = UIColor.gray
                    self?.postButton.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.gray], for: UIControlState.normal)
                }
            })
            .addDisposableTo(disposeBag)
        
        viewModel
            .textViewPlaceHolderIsHidden
            .drive(textViewPlaceHolderLabel.rx.isHidden)
            .addDisposableTo(disposeBag)
    }
}
