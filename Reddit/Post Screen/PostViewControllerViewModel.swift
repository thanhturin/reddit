//
//  PostViewControllerViewModel.swift
//  Reddit
//
//  Created by Thanh KFit on 5/18/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

enum TextViewState {
    case empty
    case exceed
    case normal
    
    var allowPost: Bool {
        switch self {
        case .empty, .exceed:
            return false
        case .normal:
            return true
        }
    }
    
    var popupMessage: String {
        switch self {
        case .empty:
            return "Your post can not be empty"
        case .exceed:
            return "Your post can not exceed 255 characters"
        case .normal:
            return ""
        }
    }
    
}

final class PostViewControllerViewModel: ViewModel {
    let postButtonIsEnable = Variable(false)
    let textViewText = Variable("")
    let textViewChecking = Variable(TextViewState.empty)
    let didTapPostButton = PublishSubject<Void>()
    let resetTextView = PublishSubject<Void>()
    
    // Fake place holder
    let textViewPlaceHolderIsHidden: Driver<Bool>
    
    init(
        feedVotingService: FeedVotingService = feedVotingServiceSharing
        ) {
        
        // Show place holder if the textView is empty
        textViewPlaceHolderIsHidden = textViewText.asDriver().map({ (text: String) -> Bool in
            return !text.isEmpty
        })
        super.init()
        
        // Checking the textField length
        textViewText.asObservable()
            .map({ (content: String) -> TextViewState in
                if content.isEmpty {
                    return .empty
                }
                else if content.characters.count >= 255 {
                    return .exceed
                }
                else {
                    return .normal
                }
            }).bind(to: textViewChecking)
            .addDisposableTo(disposeBag)
        
        textViewChecking.asObservable()
            .map({ (state: TextViewState) -> Bool in
                return state.allowPost
            }).bind(to: postButtonIsEnable)
            .addDisposableTo(disposeBag)
        
        didTapPostButton
            .subscribe(onNext: { [weak self] () in
                guard let strongSelf = self else {
                    return
                }
                feedVotingService.addNewFeed(strongSelf.textViewText.value)
                strongSelf.resetTextView.onNext()
            }).addDisposableTo(disposeBag)
    }
}
