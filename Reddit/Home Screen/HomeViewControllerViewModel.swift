//
//  HomeViewControllerViewModel.swift
//  Reddit
//
//  Created by Thanh KFit on 5/18/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class HomeViewControllerViewModel: ViewModel {
    
    let feedsDisplay = Variable<[Feed]>([])
    let didTapAutoPostButton = PublishSubject<Void>()
    let emptyFeedViewIsHidden: Driver<Bool>
    init(
        feedVotingService: FeedVotingService = feedVotingServiceSharing
        ) {
        emptyFeedViewIsHidden = feedsDisplay
            .asDriver()
            .map({ (feeds: [Feed]) -> Bool in
                return !feeds.isEmpty
            })
        
        super.init()
        
        // Get the first 20 objects and bind it to feedsDisplay and then feedsDisplay will bind it to tableView
        allFeedsVariableSharing.asObservable()
            .map { (feeds: [Feed]) -> [Feed] in
                guard !feeds.isEmpty else {
                    return []
                }
                let maxIndex = min(feeds.count - 1, 19)
                return Array(feeds[0...maxIndex])
            }.bind(to: self.feedsDisplay)
            .addDisposableTo(disposeBag)
        
        didTapAutoPostButton
            .subscribe(onNext: { () in
                feedVotingService.addNewAutoPostFeed()
            }).addDisposableTo(disposeBag)
    }
}

