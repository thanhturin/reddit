//
//  HomeViewController.swift
//  Reddit
//
//  Created by Thanh KFit on 5/18/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class HomeViewController: ViewController {
    
    var viewModel: HomeViewControllerViewModel! = HomeViewControllerViewModel()
    
    @IBOutlet weak var tableView: UITableView! 
    
    @IBAction func didTapAutoPostButton(_ sender: Any) {
        viewModel.didTapAutoPostButton.onNext()
    }
    
    @IBOutlet weak var emptyFeedView: UIView!
    
    @IBOutlet weak var addNewPostButton: UIButton!
    
    @IBAction func didTapAddNewPostButton(_ sender: Any) {
        navigationController?.tabBarController?.selectedIndex = 1
    }
    
    @IBOutlet weak var emptyFeedMessageLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        bind() 
    }
    
    func setup() {
        addNewPostButton.layer.borderWidth = 1.0
        addNewPostButton.layer.cornerRadius = 3.0
        addNewPostButton.layer.borderColor = UIColor.redditBlue().cgColor
        
        emptyFeedMessageLabel.text = "There is no new feed.\nWe are waiting for your first post :)"
        
        tableView.register(UINib.init(nibName: FeedTableViewCell.typeName, bundle: nil), forCellReuseIdentifier: FeedTableViewCell.typeName)
        tableView.estimatedRowHeight = 110
    }
}

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.feedsDisplay.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let feed = allFeedsVariableSharing.value[indexPath.row]
        let cellViewModel = FeedTableViewCellViewModel(feed: feed)
        let cell = tableView.dequeueReusableCell(withIdentifier: FeedTableViewCell.typeName, for: indexPath) as! FeedTableViewCell
        cell.viewModel = cellViewModel
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

extension HomeViewController: ViewModelBindable {
    // Binding from ViewModel to View
    func bind() {
        
        // Refresh tableview with animation when the viewModel receive any update
        viewModel
            .feedsDisplay
            .asDriver()
            .skip(1)
            .drive(onNext: { [weak self] (_) in
                guard let strongSelf = self else {
                    return
                }
                
                UIView.transition(with: strongSelf.tableView, duration: 0.2, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {
                    strongSelf.tableView.reloadData()
                }, completion: nil)
            })
            .addDisposableTo(disposeBag)
        
        viewModel
            .emptyFeedViewIsHidden
            .drive(emptyFeedView.rx.isHidden)
            .addDisposableTo(disposeBag)
    }
}
