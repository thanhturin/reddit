//
//  FeedTableViewCell.swift
//  Reddit
//
//  Created by Thanh KFit on 5/17/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class FeedTableViewCell: TableViewCell {
    
    @IBOutlet weak var contentLabel: UILabel!
    
    @IBOutlet weak var voteCountLabel: UILabel!
    
    var viewModel: FeedTableViewCellViewModel! {
        didSet {
            bind()
        }
    }
    
    @IBOutlet weak var upVoteButton: UIButton!
    
    @IBOutlet weak var downVoteButton: UIButton!
    
    @IBAction func didTapUpVoteButton(_ sender: Any) {
        viewModel.voteCountVariable.value = viewModel.voteCountVariable.value + 1
        
        let origin = upVoteButton.bounds.origin
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [UIViewAnimationOptions.layoutSubviews], animations: {
            self.upVoteButton.bounds.origin = CGPoint(x: origin.x, y: origin.y + 30)
        }, completion: { (finished) in
            if finished {
                self.upVoteButton.bounds.origin = origin
                self.viewModel.didTapUpVoteButton.onNext()
            }
        })
    }
    
    @IBAction func didTapDownVoteButton(_ sender: Any) {
        viewModel.voteCountVariable.value = viewModel.voteCountVariable.value - 1
        
        let origin = downVoteButton.bounds.origin
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [UIViewAnimationOptions.layoutSubviews], animations: {
            self.downVoteButton.bounds.origin = CGPoint(x: origin.x, y: origin.y + 30)
        }, completion: { (finished) in
            if finished {
                self.downVoteButton.bounds.origin = origin
                self.viewModel.didTapDownVoteButton.onNext()
            }
        })
    }
}

extension FeedTableViewCell: ViewModelBindable {
    // Binding from ViewModel to View
    func bind() {
        contentLabel.text = viewModel.contentString
        voteCountLabel.text = viewModel.voteCountString
        
        // Update the voteCountLabel if any update from ViewModel
        viewModel.voteCountVariable
            .asDriver()
            .skip(1)
            .map({"\($0)"})
            .drive(voteCountLabel.rx.text)
            .addDisposableTo(disposeBag)
    }
}
