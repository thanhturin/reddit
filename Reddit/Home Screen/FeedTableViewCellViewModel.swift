//
//  Created by Thanh KFit on 5/17/17.
//    Copyright © 2017 Thanh KFit. All rights reserved.
//  See LICENSE.txt for license information
//

import Foundation
import RxSwift
import RxCocoa

class FeedTableViewCellViewModel: ViewModel {
    
    // MARK- Output
    let contentString: String
    let voteCountString: String
    
    // Use to update view immediately
    let voteCountVariable: Variable<Int>
    let didTapUpVoteButton = PublishSubject<Void>()
    let didTapDownVoteButton = PublishSubject<Void>()
    
    init(
        feed: Feed
        , feedVotingService: FeedVotingService = feedVotingServiceSharing
        ) {
        self.contentString = feed.content
        self.voteCountString = "\(feed.voteCount)"
        self.voteCountVariable = Variable(feed.voteCount)
        
        super.init()
        
        didTapUpVoteButton
            .subscribe(onNext: { () in
                feedVotingServiceSharing.updateFeed(feed.id, action: VoteAction.upvote)
            })
            .addDisposableTo(disposeBag)
        
        didTapDownVoteButton
            .subscribe(onNext: { () in
                feedVotingServiceSharing.updateFeed(feed.id, action: VoteAction.downvote)
            })
            .addDisposableTo(disposeBag)
    }
    
}
