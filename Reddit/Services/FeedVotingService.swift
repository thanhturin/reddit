//
//  FeedVotingService.swift
//  Reddit
//
//  Created by Thanh KFit on 5/19/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation
import RxSwift

enum VoteAction {
    case upvote
    case downvote
    
    var voteEffectiveValue: Int {
        switch self {
        case .upvote:
            return 1
        case .downvote:
            return -1
        }
    }
}

protocol FeedVotingService {
    func addNewAutoPostFeed()
    func addNewFeed(_ content: String)
    func updateFeed(_ feedId: Int, action: VoteAction)
}

final class FeedVotingServiceDefault: FeedVotingService {
    
    let allFeedsVariableOwner: Variable<[Feed]>
    
    init(allFeedsVariableOwner: Variable<[Feed]> = allFeedsVariableSharing) {
        self.allFeedsVariableOwner = allFeedsVariableOwner
    }
    
    func addNewAutoPostFeed() {
        var feeds = allFeedsVariableOwner.value
        let newFeed = Feed.autoCreatFeed()
        feeds.append(newFeed)
        let sortedFeedList = sortFeedList(feeds: feeds)
        allFeedsVariableOwner.value = sortedFeedList
    }
    
    func addNewFeed(_ content: String) {
        var feeds = allFeedsVariableOwner.value
        let newFeed = Feed(id: feeds.count, content: content, voteCount: 0)
        feeds.append(newFeed)
        let sortedFeedList = sortFeedList(feeds: feeds)
        allFeedsVariableOwner.value = sortedFeedList
    }
    
    func updateFeed(_ feedId: Int, action: VoteAction) {
        var feeds = allFeedsVariableOwner.value
        
        // Try to find and replace the old feed with the updating feed.
        for i in 0...feeds.count - 1 {
            let currentFeed = feeds[i]
            if currentFeed.id == feedId {
                let updatingFeed = Feed(previousFeed: currentFeed, action: action)
                feeds[i] = updatingFeed
                break
            }
        }
        
        let sortedFeedList = sortFeedList(feeds: feeds)
        allFeedsVariableOwner.value = sortedFeedList
    }
    
    // Sort result by vote count. If they have same vote count then sort by Id.
    func sortFeedList(feeds: [Feed]) -> [Feed] {
        let sortedFeedList = feeds.sorted { (lhs, rhs) -> Bool in
            if lhs.voteCount == rhs.voteCount {
                return lhs.id < rhs.id
            }
            return lhs.voteCount > rhs.voteCount
        }
        
        return sortedFeedList
    }
    
}
