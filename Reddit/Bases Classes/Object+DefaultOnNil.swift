//
//  Object+DefaultOnNil.swift
//  Reddit
//
//  Created by Thanh KFit on 5/19/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation

extension Optional where Wrapped == String {
    func emptyOnNil() -> String {
        return self ?? ""
    }
}
