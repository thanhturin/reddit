//
//  UIColor+Extension.swift
//  Reddit
//
//  Created by Thanh KFit on 5/20/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    public class func redditBlue() -> UIColor {
        return UIColor(red: 50.0/255.0, green: 150.0/255.0, blue: 250.0/255.0, alpha: 1.0)
    }
}
