//
//  TypeName.swift
//  Reddit
//
//  Created by Thanh KFit on 5/19/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation

protocol TypeName {
    static var typeName: String { get }
}

// Swift Objects
extension TypeName {
    static var typeName: String {
        let type = String(describing: self)
        return type
    }
}

extension NSObject: TypeName {}
