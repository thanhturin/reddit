//
//  ViewModelBindable.swift
//  Reddit
//
//  Created by Thanh KFit on 5/18/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation

protocol ViewModelBindable {
    associatedtype ViewModelType: ViewModel
    var viewModel: ViewModelType! { get set }
    func bind()
}
