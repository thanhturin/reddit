//
//  ViewModel.swift
//  Reddit
//
//  Created by Thanh KFit on 5/18/17.
//  Copyright © 2017 Thanh KFit. All rights reserved.
//

import Foundation
import RxSwift

class ViewModel: NSObject {
    var disposeBag = DisposeBag()

    fileprivate static var activeViewModelUuid: String = Foundation.UUID().uuidString
    func setAsActive() {
        ViewModel.activeViewModelUuid = UUID
    }

    var isActive: Bool {
        return ViewModel.activeViewModelUuid == UUID
    }

    let UUID = Foundation.UUID().uuidString

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
